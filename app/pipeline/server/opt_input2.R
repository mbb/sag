RV_input2 <- reactiveValues(data = data.frame())

output$report_input2 <- DT::renderDataTable({
	return(RV_input2$data)
}, filter="top", escape = FALSE,selection = list(mode="single",target = "cell"), rownames= FALSE,server = TRUE)

#' Event when use runApp button
observeEvent(input$runApp, {

# Params 
	path_param <- "/home/jimmy/jimmy/projets/sag/app/pipeline/params/params_input2.yml"

	res <- ""

	# Panel : informations_user

	# Tool : D

	if(input$selectinformations_user == "D") {
		if(!is.na(as.numeric(input$first_name))) {
			res <- paste(res, "first_name:", input$first_name, "\n", sep = " ")
		} else {
			res <- paste(res, "first_name:", paste0('"', input$first_name, '"'), "\n", sep = " ")
		}	

		if(!is.na(as.numeric(input$last_name))) {
			res <- paste(res, "last_name:", input$last_name, "\n", sep = " ")
		} else {
			res <- paste(res, "last_name:", paste0('"', input$last_name, '"'), "\n", sep = " ")
		}	

	}

	write(res, file=path_param)

	system(paste(" pwd", "", input$first_name ,  "", input$last_name , sep = " " ))

	RV_input2$data <- read.csv("/home/jimmy/jimmy/projets/sag/app/cars.csv", header = TRUE)
})


