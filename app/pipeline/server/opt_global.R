#' Event when use last_name button
observeEvent(input$runAllPipeline, {

	path_param <- "/home/jimmy/jimmy/projets/sag/app/pipeline/params/params.yml"

	res <- ""# 	Page : inputs
		res <- paste(res, "outil_inputs_panel1:", paste0('"', input$selectpanel1, '"'), "\n", sep = " ")
		if(!is.na(as.numeric(input$param1))) {
			res <- paste(res, "param1:", input$param1, "\n", sep = " ")
		} else {
			res <- paste(res, "param1:", paste0('"', input$param1, '"'), "\n", sep = " ")
		}	

		res <- paste(res, "param2:", input$param2, "\n", sep = " ")	

		res <- paste(res, "param3_min:", input$param3[1], "\n", sep = " ")
		res <- paste(res, "param3_max:", input$param3[2], "\n", sep = " ")
		res <- paste(res, "param3_step:", "0.1", "\n", sep = " ")
	

		if(input$param8) {
			res <- paste(res, "param8:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "param8:", "false", "\n", sep = " ")
		}	

		if(input$param9) {
			res <- paste(res, "param9:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "param9:", "false", "\n", sep = " ")
		}	

		res <- paste0(res, " mychoose2" + "_left: [")
		if(length(input$mychoose2$left) > 0) {
		for(x in 1:length(input$mychoose2$left)) {
			res <- paste0(res, '"', input$mychoose2$left[[x]], '"')
			if(x < length(input$mychoose2$left)) {
				res <- paste0(res, ", ")
			}
		}
		}		
		res <- paste0(res, "]", "\n")

		res <- paste0(res, " mychoose2" + "_right: [")
		if(length(input$mychoose2$right) > 0) {
		for(x in 1:length(input$mychoose2$right)) {
			res <- paste0(res, '"', input$mychoose2$right[[x]], '"')
			if(x < length(input$mychoose2$right)) {
				res <- paste0(res, ", ")
			}
		}
		}		
		res <- paste0(res, "]", "\n")

		res <- paste(res, "outil_inputs_panel1:", paste0('"', input$selectpanel1, '"'), "\n", sep = " ")
		if(!is.na(as.numeric(input$param4))) {
			res <- paste(res, "param4:", input$param4, "\n", sep = " ")
		} else {
			res <- paste(res, "param4:", paste0('"', input$param4, '"'), "\n", sep = " ")
		}	

		if(!is.na(as.numeric(input$param5))) {
			res <- paste(res, "param5:", input$param5, "\n", sep = " ")
		} else {
			res <- paste(res, "param5:", paste0('"', input$param5, '"'), "\n", sep = " ")
		}	

		res <- paste(res, "outil_inputs_panel2:", paste0('"', input$selectpanel2, '"'), "\n", sep = " ")
		if(!is.na(as.numeric(input$param10))) {
			res <- paste(res, "param10:", input$param10, "\n", sep = " ")
		} else {
			res <- paste(res, "param10:", paste0('"', input$param10, '"'), "\n", sep = " ")
		}	

			res <- paste(res, "param7:", paste0('"', input$param7$datapath, '"'), "\n", sep = " ")	

		res <- paste(res, "outil_inputs_panel3:", paste0('"', input$selectpanel3, '"'), "\n", sep = " ")
		if(!is.na(as.numeric(input$inputs_param1))) {
			res <- paste(res, "inputs_param1:", input$inputs_param1, "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param1:", paste0('"', input$inputs_param1, '"'), "\n", sep = " ")
		}	

		res <- paste(res, "inputs_param2:", input$inputs_param2, "\n", sep = " ")	

		res <- paste(res, "inputs_param3_min:", input$inputs_param3[1], "\n", sep = " ")
		res <- paste(res, "inputs_param3_max:", input$inputs_param3[2], "\n", sep = " ")
		res <- paste(res, "inputs_param3_step:", "0.1", "\n", sep = " ")
	

		if(input$inputs_param8) {
			res <- paste(res, "inputs_param8:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param8:", "false", "\n", sep = " ")
		}	

		if(input$inputs_param9) {
			res <- paste(res, "inputs_param9:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param9:", "false", "\n", sep = " ")
		}	

		res <- paste(res, "outil_inputs_panel3:", paste0('"', input$selectpanel3, '"'), "\n", sep = " ")
		if(!is.na(as.numeric(input$inputs_param1))) {
			res <- paste(res, "inputs_param1:", input$inputs_param1, "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param1:", paste0('"', input$inputs_param1, '"'), "\n", sep = " ")
		}	

		if(input$inputs_param8) {
			res <- paste(res, "inputs_param8:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param8:", "false", "\n", sep = " ")
		}	

		if(input$inputs_param9) {
			res <- paste(res, "inputs_param9:", "true", "\n", sep = " ")
		} else {
			res <- paste(res, "inputs_param9:", "false", "\n", sep = " ")
		}	

# 	Page : input2
		res <- paste(res, "outil_input2_informations_user:", paste0('"', input$selectinformations_user, '"'), "\n", sep = " ")
		if(!is.na(as.numeric(input$first_name))) {
			res <- paste(res, "first_name:", input$first_name, "\n", sep = " ")
		} else {
			res <- paste(res, "first_name:", paste0('"', input$first_name, '"'), "\n", sep = " ")
		}	

		if(!is.na(as.numeric(input$last_name))) {
			res <- paste(res, "last_name:", input$last_name, "\n", sep = " ")
		} else {
			res <- paste(res, "last_name:", paste0('"', input$last_name, '"'), "\n", sep = " ")
		}	

	write(res, file=path_param)

})


