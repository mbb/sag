tabinput2 = fluidPage(

box(title = "Parameters :", width = 5, status = "primary", collapsible = TRUE, solidHeader = TRUE,

tabsetPanel(type = "tabs",

tabPanel("Biblio", height = "300px", style = "overflow: hidden; overflow-y:scroll", br(),
	selectInput("selectinformations_user", label = "Select the tool to use : ", choices = list("D" = "D")),
	conditionalPanel(
	condition = "input.selectinformations_user == 'D'",
	box(title = "Tool D :", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		textInput("first_name", label = "First name :", value = "", width = "auto"),

		textInput("last_name", label = "Last name :", value = "", width = "auto")

	))

)

)

),

box(title = "Results :", width = 7, status = "primary", collapsible = TRUE, solidHeader = TRUE, style = "overflow: hidden; overflow-x:scroll",

	actionButton("runApp", "Run",  icon("play"), class="btn btn-primary"),

	br(), br(), DT::dataTableOutput("report_input2")

)

)


